import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Collection;
import java.util.HashSet;
import java.awt.event.ActionEvent;

public class Test_task_GUI {

    private JFrame Test_Task_Final_with_GUI;
    private JTextField FieldForInFile;
    private JTextField FieldForDict;
    private final int MAX_FILE_SIZE = 2097152; //Максимальный размер входящего файла.

    private JTextField N_countString;
    public String countString = "";
    private JTextField FieldForOutFile;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Test_task_GUI window = new Test_task_GUI();
                    window.Test_Task_Final_with_GUI.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public Test_task_GUI() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        Test_Task_Final_with_GUI = new JFrame();
        Test_Task_Final_with_GUI.setTitle("Test_Task_Final_with_GUI");
        Test_Task_Final_with_GUI.setBounds(100, 100, 450, 300);
        Test_Task_Final_with_GUI.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Test_Task_Final_with_GUI.getContentPane().setLayout(null);

        JLabel lblInfile = new JLabel("\u0412\u044B\u0431\u0435\u0440\u0438\u0442\u0435 \u043E\u0431\u0440\u0430\u0431\u0430\u0442\u044B\u0432\u0430\u0435\u043C\u044B\u0439 \u0444\u0430\u0439\u043B");
        lblInfile.setBounds(10, 10, 241, 23);
        Test_Task_Final_with_GUI.getContentPane().add(lblInfile);

        FieldForInFile = new JTextField();
        FieldForInFile.setBounds(10, 30, 315, 26);
        Test_Task_Final_with_GUI.getContentPane().add(FieldForInFile);
        FieldForInFile.setColumns(10);

        JButton input = new JButton("\u041E\u0431\u0437\u043E\u0440");
        input.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                JFileChooser fileopen = new JFileChooser();
                fileopen.setCurrentDirectory(new File("."));
                int ret = fileopen.showDialog(null, "Выберите файл для обработки");


                if (ret == JFileChooser.APPROVE_OPTION) {
                    File inFile = fileopen.getSelectedFile();
                    FieldForInFile.setText(inFile.getAbsolutePath());
                }
            }
        });
        input.setBounds(335, 30, 89, 26);
        Test_Task_Final_with_GUI.getContentPane().add(input);

        JButton done = new JButton("\u0412\u044B\u043F\u043E\u043B\u043D\u0438\u0442\u044C");
        done.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.print(FieldForInFile.getText());
                /*Объявление переменных*/
                countString = N_countString.getText();
                int N = Integer.parseInt(countString);
                Collection<String> dict = new HashSet<String>();//Коллекция для хранения словаря
                //String directory = System.getProperty("user.dir");//Получение текущей иректории
                BufferedReader br = null;
                BufferedReader in = null;
                BufferedWriter out = null;
                int linesCount = 0; //Счетчик текущего количества строк в файле
                int point = 0; //Позиция символа точки
                int filesCount = 1; //Текущий кусок файла
                int proposalCount = 0; //Количество строк в предложении
                Converter conv = new Converter();
                StringBuilder proposal = new StringBuilder();
                String strLine; //Считываемая из текста строка

                String close = ""
                        + "</body>"
                        + "</html>";

                String title = ""
                        + "<html>\n"
                        + "<head>\n"
                        + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />"
                        + "<title>HTML Document</title>"
                        + "</head>"
                        + "<body>";

                //Читаем словарь
                File fileDict = new File(FieldForDict.getText());
                if (fileDict.length() < MAX_FILE_SIZE) {
                    try {
                        br = new BufferedReader(new InputStreamReader(new FileInputStream(fileDict), "UTF-8"));
                        while ((strLine = br.readLine()) != null) {
                            dict.add(strLine.toString());
                        }
                        if (dict.isEmpty()) {
                            JOptionPane.showMessageDialog(null, "Внимание!!\n Файл словаря пуст.");
                        }
                    } catch (FileNotFoundException e1) {
                        JOptionPane.showMessageDialog(null, "Ошибка {Блок 1} \nОтсутствует файл словаря.\nУбедитесь, что файл имеет имя File_dict и находится в одной директории с исполняемым файлом!!\n");
                    } catch (Throwable e1) {
                        JOptionPane.showMessageDialog(null, "Ошибка {Блок 2}\n" + e1.getMessage());
                    } finally {
                        if (br != null) {
                            try {
                                br.close();
                            } catch (Throwable e1) {
                                JOptionPane.showMessageDialog(null, "Ошибка {Блок 3}\n" + e1.getMessage());
                            }
                        }
                    }
                } else JOptionPane.showMessageDialog(null, "Ошибка {Блок 4}\n Файл словаря не должен превышать 2Мб");


                //Читаем и обрабатываем файл

                File inFile = new File(FieldForInFile.getText());
                String nameOutFile = FieldForOutFile.getText();
                if (inFile.length() < MAX_FILE_SIZE) {
                    File outFile = new File(nameOutFile);
                    try {
                        try {
                            in = new BufferedReader(new InputStreamReader(new FileInputStream(inFile), "UTF-8"));
                            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile + "_1.html"), "UTF-8"));
                            out.write(title);
                        } catch (Throwable e1) {
                            JOptionPane.showMessageDialog(null, "Ошибка {Блок 5} \nОтсутствует обрабатываемый файл.\nУбедитесь, что файл имеет имя (inFile.txt) и  находится в одной директории с исполняемым файлом!!\n");
                            e1.getMessage();
                        }

                        try {
                            while ((strLine = in.readLine()) != null) {
                                point = strLine.lastIndexOf('.');
                                if (point == -1) {
                                    strLine = conv.makeString(strLine, dict);
                                    proposal.append(strLine + "<br />");
                                    ++proposalCount; //Увеличиваеме счетчик количества строк в предложении
                                } else {
                                    proposal.append(conv.makeString(strLine.substring(0, point + 1), dict));
                                    out.write(proposal.toString());
                                    proposalCount = 0; //Обнуление количества предложений
                                    proposal = new StringBuilder();
                                    if (point != strLine.length() - 1) {
                                        //Если точка - не последний символ в предложении, то добавление оставшейся части предложения.
                                        proposal.append(conv.makeString(strLine.substring(point + 1, strLine.length()), dict) + "<br />");
                                        proposalCount = 1;
                                    } else {
                                        //Если точка - последний символ предложения, но не файла, то просто перевод строки
                                        if (linesCount != N - 1) {
                                            proposal.append("<br />");
                                        }
                                    }
                                }

                                ++linesCount;//Увеличиваем счетчик количества строк в файле

                                if (linesCount == N) {
                                    out.write(close);
                                    out.close();
                                    //Если существуют предложения длиннее максимально разрешенного лимита
                                    if (proposalCount >= N) {
                                        outFile.delete();
                                        JOptionPane.showMessageDialog(null, "Ошибка {Блок 6}\n В обрабатываемом тексте присутствует предложение, состоящее более чем из " + N + " строк. \n Работа программы будет остановлена!!!");
                                        break;
                                    }
                                    linesCount = proposalCount;
                                    outFile = new File(nameOutFile + String.valueOf(++filesCount) + "_.html");
                                    out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile), "UTF-8"));
                                    out.write(title);
                                }
                            }
                        } catch (NullPointerException e1) {
                            JOptionPane.showMessageDialog(null, "Ошибка {Блок 7}\n Обрабатываемый поток пуст.");
                        } catch (Throwable e1) {

                        }

                    } finally {
                        if (in != null) {
                            try {
                                in.close();
                            } catch (Throwable e1) {
                                JOptionPane.showMessageDialog(null, "Ошибка {Блок 8}\n" + e1.getMessage());
                            }
                        }
                        if (out != null) {
                            try {
                                out.close();
                            } catch (Throwable e1) {
                                JOptionPane.showMessageDialog(null, "Ошибка {Блок 9}\n" + e1.getMessage());
                            }
                        }
                    }
                }
                JOptionPane.showMessageDialog(null, "Работа программы завершена.");


            }
        });
        done.setBounds(10, 227, 89, 23);
        Test_Task_Final_with_GUI.getContentPane().add(done);

        JLabel label = new JLabel("\u0412\u044B\u0431\u0435\u0440\u0438\u0442\u0435 \u0444\u0430\u0439\u043B \u0441\u043B\u043E\u0432\u0430\u0440\u044F");
        label.setBounds(10, 60, 122, 23);
        Test_Task_Final_with_GUI.getContentPane().add(label);

        FieldForDict = new JTextField();
        FieldForDict.setBounds(10, 85, 315, 26);
        Test_Task_Final_with_GUI.getContentPane().add(FieldForDict);
        FieldForDict.setColumns(10);

        JButton button = new JButton("\u041E\u0431\u0437\u043E\u0440");
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                JFileChooser fileopen = new JFileChooser();
                fileopen.setCurrentDirectory(new File("."));
                int ret = fileopen.showDialog(null, "Выберите файл словаря");


                if (ret == JFileChooser.APPROVE_OPTION) {
                    File inFile = fileopen.getSelectedFile();
                    FieldForDict.setText(inFile.getAbsolutePath());
                }
            }
        });
        button.setBounds(335, 85, 89, 26);
        Test_Task_Final_with_GUI.getContentPane().add(button);

        JLabel label_1 = new JLabel("\u0423\u043A\u0430\u0436\u0438\u0442\u0435 \u043A\u043E\u043B\u0438\u0447\u0435\u0441\u0442\u0432\u043E \u0441\u0442\u0440\u043E\u043A \u0432 html \u0444\u0430\u0439\u043B\u0435");
        label_1.setBounds(10, 177, 251, 23);
        Test_Task_Final_with_GUI.getContentPane().add(label_1);

        N_countString = new JTextField();
        N_countString.setBounds(271, 175, 54, 26);
        Test_Task_Final_with_GUI.getContentPane().add(N_countString);
        N_countString.setColumns(10);

        JLabel lblNewLabel = new JLabel("\u0423\u043A\u0430\u0436\u0438\u0442\u0435 \u0438\u043C\u044F html \u0444\u0430\u0439\u043B\u0430");
        lblNewLabel.setBounds(10, 115, 315, 23);
        Test_Task_Final_with_GUI.getContentPane().add(lblNewLabel);

        FieldForOutFile = new JTextField();
        FieldForOutFile.setBounds(10, 140, 315, 26);
        Test_Task_Final_with_GUI.getContentPane().add(FieldForOutFile);
        FieldForOutFile.setColumns(10);

        JButton btnNewButton = new JButton("\u041E\u0431\u0437\u043E\u0440");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {


                JFileChooser fileopen = new JFileChooser();
                fileopen.setCurrentDirectory(new File("."));
                int ret = fileopen.showDialog(null, "Выбрать имя выходного файла");

                if (ret == JFileChooser.APPROVE_OPTION) {
                    File file = fileopen.getSelectedFile();
                    FieldForOutFile.setText(file.getAbsolutePath());
                }
            }

        });
        btnNewButton.setBounds(335, 140, 89, 26);
        Test_Task_Final_with_GUI.getContentPane().add(btnNewButton);
    }
}
