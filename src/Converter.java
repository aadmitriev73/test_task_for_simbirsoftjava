import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Converter {

    public String makeString(String source, Collection<String> dict) {
        Pattern p;
        Matcher m;
        String arr[] = source.split(" ");
        for (int j = 0; j < arr.length; j++) {
            if (dict.contains(arr[j])) {//Сравниваем есть ли данное слово в словаре
                p = Pattern.compile(arr[j]);
                m = p.matcher(source);
                source = m.replaceAll("<b><i>" + arr[j] + "</b></i>"); //Заменить слово в предложении на выделенное
            }
        }
        return source;
    }
}